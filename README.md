# dev_vagabuntu

Cria uma VM Ubuntu 20.04 com ferramentas IaC instaladas para desenvolvimento.

Instala Ansible, opentofu e Packer


Requer o plugin vagrant-vbguest (para vboxfs). Para instalar:

```
vagrant plugin install vagrant-vbguest
``` 

Em caso de problema:
set VAGRANT_PREFER_SYSTEM_BIN=0
